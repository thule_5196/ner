package com.sample;

import com.sample.model.Message;
import com.sample.model.Segment;
import com.sample.model.Token;
import com.sample.service.DataPreprocessingService;
import com.sample.service.FileWriterService;
import org.junit.Before;
import org.junit.Test;
import org.kie.api.KieBase;
import org.kie.api.KieServices;
import org.kie.api.definition.KiePackage;
import org.kie.api.definition.rule.Rule;
import org.kie.api.runtime.KieContainer;
import org.kie.api.runtime.KieSession;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.xml.parsers.*;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.*;

import static org.junit.Assert.assertTrue;

public class RuleTest {
    static final Logger LOG = LoggerFactory.getLogger(RuleTest.class);
    List<String> countriesList;
    List<String> orgList;
    KieSession session;
    @Before
    public void setUp(){
        KieServices kieServices = KieServices.Factory.get();

        KieContainer kContainer = kieServices.getKieClasspathContainer();

        LOG.info("Creating kieBase");
        KieBase kieBase = kContainer.getKieBase();

        LOG.info("There should be rules: ");
        for ( KiePackage kp : kieBase.getKiePackages() ) {
            for (Rule rule : kp.getRules()) {
                LOG.info("kp " + kp + " rule " + rule.getName());
            }
        }

        LOG.info("Creating kieSession");
        session = kieBase.newKieSession();

        LOG.info("Now running data");

        Set<String> nePER= new HashSet<String>();
        Set<String> neLOC= new HashSet<String>();
        Set<String> neORG= new HashSet<String>();

        session.setGlobal("nePER", nePER);
//        session.setGlobal("neLOC", neLOC);
//        session.setGlobal("neORG", neORG);

        List<String> conjList = new ArrayList<>();
        try {
            File myObj = new File(getClass().getClassLoader().getResource("conjunction.txt").getFile());
            Scanner myReader = new Scanner(myObj);
            while (myReader.hasNextLine()) {
                String data = myReader.nextLine();
                conjList.add(data);
            }
            myReader.close();
        } catch (FileNotFoundException e) {
            System.out.println("An error occurred.");
            e.printStackTrace();
        }
        session.setGlobal("conjList", conjList);

        List<String> pronounList = new ArrayList<>();
        try {
            File myObj = new File(getClass().getClassLoader().getResource("pronouns.txt").getFile());
            Scanner myReader = new Scanner(myObj);
            while (myReader.hasNextLine()) {
                String data = myReader.nextLine();
                pronounList.add(data);
            }
            myReader.close();
        } catch (FileNotFoundException e) {
            System.out.println("An error occurred.");
            e.printStackTrace();
        }
        session.setGlobal("pronounsLíst", pronounList);

        List<String> adverbsList = new ArrayList<>();
        try {
            File myObj = new File(getClass().getClassLoader().getResource("adverbs.txt").getFile());
            Scanner myReader = new Scanner(myObj);
            while (myReader.hasNextLine()) {
                String data = myReader.nextLine();
                adverbsList.add(data);
            }
            myReader.close();
        } catch (FileNotFoundException e) {
            System.out.println("An error occurred.");
            e.printStackTrace();
        }
        session.setGlobal("adverbsList", adverbsList);

        List<String> personNameList = new ArrayList<>();
        try {
            File myObj = new File(getClass().getClassLoader().getResource("person_name.txt").getFile());
            Scanner myReader = new Scanner(myObj);
            while (myReader.hasNextLine()) {
                String data = myReader.nextLine();
                personNameList.add(data);
            }
            myReader.close();
        } catch (FileNotFoundException e) {
            System.out.println("An error occurred.");
            e.printStackTrace();
        }
        session.setGlobal("personList", personNameList);


        countriesList = new ArrayList<>();
        try {
            File myObj = new File(getClass().getClassLoader().getResource("countries.txt").getFile());
            Scanner myReader = new Scanner(myObj);
            while (myReader.hasNextLine()) {
                String data = myReader.nextLine();
                countriesList.add(data);
            }
            myReader.close();
        } catch (FileNotFoundException e) {
            System.out.println("An error occurred.");
            e.printStackTrace();
        }
        System.out.println(countriesList.size());

        orgList = new ArrayList<>();
        try {
            File myObj = new File(getClass().getClassLoader().getResource("organizations.txt").getFile());
            Scanner myReader = new Scanner(myObj);
            while (myReader.hasNextLine()) {
                String data = myReader.nextLine();
                orgList.add(data);
            }
            myReader.close();
        } catch (FileNotFoundException e) {
            System.out.println("An error occurred.");
            e.printStackTrace();
        }
        System.out.println(orgList.size());

    }

    @Test
    public void testCSV() throws IOException {
        FileWriterService fileWriterService = new FileWriterService();

//        fileWriterService.writeToCSV();

        String s = "aaaab. aaaaaac.";
        System.out.println(s.replaceAll("([a-zA-Z]{1})(\\.)", "$1 $2"));
    }

    @Test
    public void testMessage() {


        Message message = new Message("test");
        session.insert(message);
        session.fireAllRules();
//        assertTrue(message.isValid());

    }

    @Test
    public void testXMLAsText() throws ParserConfigurationException, SAXException {
//
//        SAXParserFactory factory = SAXParserFactory.newInstance();
//        SAXParser saxParser = factory.newSAXParser();

        try {
            File myObj = new File(getClass().getClassLoader().getResource("newstest2013-src.vi.xml").getFile());
            Scanner myReader = new Scanner(myObj);
            while (myReader.hasNextLine()) {
                String data = myReader.nextLine();
                System.out.println(data);
            }
            myReader.close();
        } catch (FileNotFoundException e) {
            System.out.println("An error occurred.");
            e.printStackTrace();
        }
    }


    @Test
    public void testXML() throws IOException, SAXException, ParserConfigurationException {
        DataPreprocessingService dataPreprocessingService = new DataPreprocessingService();
        dataPreprocessingService.prepapre();
        assertTrue(dataPreprocessingService.getDocumentNode("seg").getLength() == 3000);

        List<Segment> segments = dataPreprocessingService.prepare();
        System.out.println(segments.get(0));
    }

    @Test
    public void testReadTextFile() throws IOException, SAXException, ParserConfigurationException {
        DataPreprocessingService dataPreprocessingService = new DataPreprocessingService();
        dataPreprocessingService.prepareTextFile();
//        assertTrue(dataPreprocessingService.getDocumentNode("seg").getLength() == 3000);

        List<Segment> segments = dataPreprocessingService.prepareTextFile();
        System.out.println(segments.get(0));
    }

    @Test
    public void testXMLLinkedList() throws IOException, SAXException, ParserConfigurationException {
        DataPreprocessingService dataPreprocessingService = new DataPreprocessingService();
        dataPreprocessingService.prepapre();
        assertTrue(dataPreprocessingService.getDocumentNode("seg").getLength() == 3000);

        List<Segment> segments = dataPreprocessingService.prepare();
//        System.out.println(segments.get(0));

        segments.get(0).getTokens().get(3).setToken("TEST_ABC");
        assertTrue(segments.get(0).getTokens().get(4).getPrefix().getToken() == "TEST_ABC");
    }

    @Test
    public void testLastNameRule(){
        String testSentence = "Nhưng ông Nguyen An la nguoi Viet và An 10,1% do ngày 1 tháng 12 và 05:52:34 và James hoạt động của Lê Văn Tám";
//        testSentence = "Thông qua Thị trưởng";
        Segment s = new Segment(testSentence);
        for (Token t : s.getTokens()){
            session.insert(t);
            session.fireAllRules();
        }
        assertTrue(s.getTokens().get(2).isNE());
        assertTrue(s.getTokens().get(8).isNE());

        String toPrint="";
        for (Token t: s.getTokens()){
            if (t.isNE()){
                toPrint = toPrint+ " "+t.getToken();
            }
            else
            {
                System.out.println(toPrint);
                toPrint="";
            }

        }
        System.out.println(toPrint);
    }

    @Test
    public void testFullRun() throws IOException, SAXException, ParserConfigurationException {
        DataPreprocessingService dataPreprocessingService = new DataPreprocessingService();
        dataPreprocessingService.prepapre();
        assertTrue(dataPreprocessingService.getDocumentNode("seg").getLength() == 3000);

        List<Segment> segments = dataPreprocessingService.prepare();
//        System.out.println(segments.get(0));
//
//        segments.get(0).getTokens().get(3).setToken("TEST_ABC");
//        assertTrue(segments.get(0).getTokens().get(4).getPrefix().getToken() == "TEST_ABC");

        for (Segment s: segments){
            for (Token t : s.getTokens()){
                session.insert(t);
                session.fireAllRules();
            }
            s.printNE();
        }
    }


    @Test
    public void testFullRun_toFile() throws IOException, SAXException, ParserConfigurationException {
        DataPreprocessingService dataPreprocessingService = new DataPreprocessingService();
        dataPreprocessingService.prepapre();
        assertTrue(dataPreprocessingService.getDocumentNode("seg").getLength() == 3000);
        BufferedWriter csvWriter = Files.newBufferedWriter(Paths.get("new.txt"), StandardCharsets.UTF_8);

        csvWriter.append("Sentence");
        csvWriter.append(",");
        csvWriter.append("NE");
        csvWriter.append(",");
        csvWriter.append("Type");
        csvWriter.append("\n");

        List<Segment> segments = dataPreprocessingService.prepare();

        for (Segment s: segments){
            for (Token t : s.getTokens()){
                session.insert(t);
                session.fireAllRules();
            }
            s.printNE_toFile(csvWriter,countriesList,orgList);
        }
        csvWriter.close();

    }

    @Test
    public void testFullRun_toFile_eval() throws IOException, SAXException, ParserConfigurationException {
        DataPreprocessingService dataPreprocessingService = new DataPreprocessingService();
//        dataPreprocessingService.prepareTextFile();
//        assertTrue(dataPreprocessingService.getDocumentNode("seg").getLength() == 3000);


        BufferedWriter csvWriter = Files.newBufferedWriter(Paths.get("new2.txt"), StandardCharsets.UTF_8);

        csvWriter.append("Sentence");
        csvWriter.append(",");
        csvWriter.append("NE");
        csvWriter.append(",");
        csvWriter.append("Type");
        csvWriter.append("\n");

        List<Segment> segments = dataPreprocessingService.prepareTextFile();

        for (Segment s: segments){
            for (Token t : s.getTokens()){
                session.insert(t);
                session.fireAllRules();
            }
            s.printNE_toFile(csvWriter,countriesList,orgList);
        }
        csvWriter.close();

    }

}