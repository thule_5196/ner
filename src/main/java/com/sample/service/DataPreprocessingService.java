package com.sample.service;

import com.sample.model.Segment;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.LinkedList;
import java.util.List;
import java.util.Scanner;

public class DataPreprocessingService {

    public DataPreprocessingService(){
        doc=null;

    }

    private Document doc;

    public void prepapre() throws ParserConfigurationException, IOException, SAXException {

        File file = new File(getClass().getClassLoader().getResource("newstest2013-src.vi.xml").getFile());
        DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
        DocumentBuilder db = dbf.newDocumentBuilder();
        doc = db.parse(file);
        doc.getDocumentElement().normalize();
        System.out.println("Root element: " + doc.getDocumentElement().getNodeName());
    }

    public NodeList getDocumentNode(String nodeName){
        return doc.getElementsByTagName(nodeName);
    }

    public List<Segment> prepare(){

        List<Segment> sentences = new LinkedList<Segment>();
        NodeList segments = this.getDocumentNode("seg");

        for (int i=0;i<segments.getLength();i++){
            Node node = segments.item(i);
//            System.out.println("\nNode Name :" + node.getNodeType());
            if (node.getNodeType() == Node.ELEMENT_NODE)
            {
                Element eElement = (Element) node;

                String sentenceStr  = eElement.getTextContent();
                sentenceStr = sentenceStr.replace(", ", " , ");
                sentenceStr = sentenceStr.replace(". ", " . ");
                sentenceStr = sentenceStr.replace(" (", " ( ");
                sentenceStr = sentenceStr.replace(") ", " ) ");
                sentenceStr = sentenceStr.replace("). ", " ). ");
                sentenceStr = sentenceStr.replaceAll("([a-zA-Z]{1})(\\.)", "$1 $2");



//                System.out.println("Sentence: "+ eElement.getTextContent());
                sentences.add(new Segment(sentenceStr));

            }
        }
        return sentences;
    }

    public List<Segment> prepareTextFile() throws FileNotFoundException {

        List<String> segments = new LinkedList<>();

        File myObj = new File(getClass().getClassLoader().getResource("testdata.txt").getFile());
        Scanner myReader = new Scanner(myObj);
        while (myReader.hasNextLine()) {
            String data = myReader.nextLine();
            segments.add(data);
        }
        myReader.close();


        List<Segment> sentences = new LinkedList<Segment>();

        for (int i=0;i<segments.size();i++){


                String sentenceStr  = segments.get(i);
                sentenceStr = sentenceStr.replace(", ", " , ");
                sentenceStr = sentenceStr.replace(". ", " . ");
                sentenceStr = sentenceStr.replace(" (", " ( ");
                sentenceStr = sentenceStr.replace(") ", " ) ");
                sentenceStr = sentenceStr.replace(")", " )");
                sentenceStr = sentenceStr.replace("). ", " ). ");
                sentenceStr = sentenceStr.replace(": ", " : ");
                sentenceStr = sentenceStr.replaceAll("([a-zA-Z]{1})(\\.)", "$1 $2");



//                System.out.println("Sentence: "+ eElement.getTextContent());
                sentences.add(new Segment(sentenceStr));

        }
        return sentences;
    }
}
