package com.sample.model;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.LinkedList;
import java.util.List;

public class Segment {
    private String sentence;
    private List<Token> tokens;

    public Segment(String sentence) {
        this.sentence = sentence;
        tokens = new LinkedList<Token>();
        String[] tokens_temp = sentence.split(" ");
        Token pre = null;
        Token post = null;
        for (int i=0; i<tokens_temp.length;i++){
            Token t = new Token(tokens_temp[i],pre);
            if(i>0){
                t.getPrefix().setSuffix(t);
            }
            tokens.add(t);
            pre = t;
        }
    }

    public Segment() {
    }

    public String getSentence() {
        return sentence;
    }

    public void setSentence(String sentence) {
        this.sentence = sentence;
    }

    public List<Token> getTokens() {
        return tokens;
    }

    public void setTokens(List<Token> tokens) {
        this.tokens = tokens;
    }

    @Override
    public String toString() {
        return "Segment{" +
                "sentence='" + sentence + '\'' +
                '}';
    }

    public void printNE(){
        String toPrint="";
        for (Token t: tokens){
            if (t.isNE()){
                toPrint = toPrint+ " "+t.getToken();
            }
            else
            {
                if (!toPrint.isEmpty()){
                    System.out.println(toPrint);
                }
                toPrint="";
            }

        }
        System.out.println(toPrint);
    }

    public void printNE_toFile(BufferedWriter csvWriter, List<String> countriesList,List<String> orgList) throws IOException {

//        FileWriter csvWriter = new FileWriter("new.csv");




        String toPrint="";
        String type = "";
        int begin = 0;
        int end = 0;
        for (int i = 0; i<this.tokens.size();i++){
            if (this.tokens.get(i).isNE()){
                toPrint = toPrint+ " "+this.tokens.get(i).getToken();
                type = this.tokens.get(i).getNeType()!= null ? this.tokens.get(i).getNeType() : "";
            }
            else
            {
                if (!toPrint.isEmpty()){
                    System.out.println(toPrint);
//                    csvWriter.append(this.sentence.replace("\"","'"));
                    end = i;
                    if(countriesList.contains(toPrint.trim())){
                        System.out.println("Found" + begin + " "+end);
                        for (int j = begin; j<=end;j++){
                            tokens.get(j).setNeType("I-LOC");
                            type = "I-LOC";
                        }
                    }
                    if(orgList.contains(toPrint.trim())){
                        System.out.println("Found" + begin + " "+end);
                        for (int j = begin; j<=end;j++){
                            tokens.get(j).setNeType("I-ORG");
                            type = "I-ORG";
                        }
                    }
                    csvWriter.append(this.sentence.replace(",",";"));

                    csvWriter.append(",");
                    csvWriter.append(toPrint.replace(",","."));
                    csvWriter.append(",");
                    csvWriter.append(type);
                    csvWriter.append("\n");
                }
                begin =i;
                toPrint="";
                type = "";
            }

        }
        if (!toPrint.isEmpty()){
            csvWriter.append(this.sentence.replace(",",";"));
            csvWriter.append(",");
            csvWriter.append(toPrint.replace(",","."));
            csvWriter.append(",");
            csvWriter.append(type);
            csvWriter.append("\n");

        }
        System.out.println(toPrint);
        csvWriter.flush();
    }


}
