package com.sample.model;

public class Token {
    private String token;
    private boolean isNE;
    private String neType;
    private Token prefix;
    private Token suffix;


    public Token() {
    }

    public Token(String token, Token prefix) {
        this.token = token;
        this.prefix = prefix;
        this.isNE = false;
//        this.suffix = suffix;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public boolean isNE() {
        return isNE;
    }

    public void setNE(boolean NE) {
        isNE = NE;
//        System.out.println(token);
    }

    public String getNeType() {
        return neType;
    }

    public void setNeType(String neType) {
        this.neType = neType;
    }

    public Token getPrefix() {
        return prefix;
    }

    public void setPrefix(Token prefix) {
        this.prefix = prefix;
    }

    public Token getSuffix() {
        return suffix;
    }

    public void setSuffix(Token suffix) {
        this.suffix = suffix;
    }

    @Override
    public String toString() {
        return "Token{" +
                "token='" + token + '\'' +
                ", isNE=" + isNE +
                ", neType='" + neType + '\'' +
                ", prefix=" + prefix +
                ", suffix=" + suffix +
                '}';
    }
    public boolean isInitCapitalize(){
        if (this.token.isEmpty()){
            return false;
        }

        if(!Character.isLetterOrDigit(this.token.charAt(0))){
            return false;
        }
        return this.token.charAt(0) == this.getToken().toUpperCase().charAt(0) && this.token.charAt(0) != this.getToken().toLowerCase().charAt(0);

    }

    public boolean isCapitalize(){
        if (this.token.isEmpty()){
            return false;
        }
        if(!Character.isLetterOrDigit(this.token.charAt(0))){
            return false;
        }

        try {
            Integer a = Integer.parseInt(this.token);
            return false;
        }catch (Exception e){
            return this.token.equals(this.getToken().toUpperCase()) && !this.token.equals(this.getToken().toLowerCase()) ;
        }
//        return this.token.equals(this.getToken().toUpperCase()) && this.token.equals(this.getToken().toLowerCase()) ;
    }

    public boolean isNumeric(){
        try {
            Double.parseDouble(token);
            return true;
        } catch(NumberFormatException e){
            return false;
        }
    }


}
