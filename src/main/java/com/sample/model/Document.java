package com.sample.model;

import java.util.LinkedList;
import java.util.List;

public class Document {

    private Segment header;
    private List<Segment> body;

    public Document(Segment header) {
        this.header = header;
        this.body = new LinkedList<Segment>();
    }

    public Segment getHeader() {
        return header;
    }

    public void setHeader(Segment header) {
        this.header = header;
    }

    public List<Segment> getBody() {
        return body;
    }

    public void setBody(List<Segment> body) {
        this.body = body;
    }
}
